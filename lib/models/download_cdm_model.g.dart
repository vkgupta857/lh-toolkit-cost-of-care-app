// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'download_cdm_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class DownloadCdmModelAdapter extends TypeAdapter<DownloadCdmModel> {
  @override
  final typeId = 1;

  @override
  DownloadCdmModel read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return DownloadCdmModel(fields[0] as String, fields[1] as int,
        fields[2] as int, fields[3] as String);
  }

  @override
  void write(BinaryWriter writer, DownloadCdmModel obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.hospitalName)
      ..writeByte(1)
      ..write(obj.isDownload)
      ..writeByte(2)
      ..write(obj.isBookmarked)
      ..writeByte(3)
      ..write(obj.hospitalAddress);
  }
}
