import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cost_of_care/models/outpatient_procedure.dart';
import 'package:cost_of_care/repository/inpatient_outpatient_procedure_repository_impl.dart';
import 'package:equatable/equatable.dart';

part 'outpatient_procedure_event.dart';
part 'outpatient_procedure_state.dart';

class OutpatientProcedureBloc
    extends Bloc<OutpatientProcedureEvent, OutpatientProcedureState> {
  InpatientOutpatientProcedureRepositoryImpl
      inpatientOutpatientProcedureRepositoryImpl;
  OutpatientProcedureBloc(this.inpatientOutpatientProcedureRepositoryImpl)
      : super(OutpatientProcedureInitial());
  get initialState => OutpatientProcedureInitial();

  @override
  Stream<OutpatientProcedureState> mapEventToState(
    OutpatientProcedureEvent event,
  ) async* {
    if (event is OutpatientProcedureFetchData) {
      yield initialState;
      bool isSaved = false;

      if (!event.isHardRefresh) {
        isSaved =
            inpatientOutpatientProcedureRepositoryImpl.checkSavedOutpatient();
      }
      if (isSaved) {
        List<OutpatientProcedure> outpatientProcedure =
            inpatientOutpatientProcedureRepositoryImpl.getSavedOutpatient();
        yield OutpatientProcedureLoadedState(outpatientProcedure);
      } else {
        try {
          yield OutpatientProcedureLoadingState();
          List<OutpatientProcedure> outpatientProcedure =
              await inpatientOutpatientProcedureRepositoryImpl
                  .getOutpatientProcedures();
          inpatientOutpatientProcedureRepositoryImpl
              .saveOutpatient(outpatientProcedure);
          yield OutpatientProcedureLoadedState(outpatientProcedure);
        } catch (e) {
          yield OutpatientProcedureErrorState(e.message);
        }
      }
    }
  }
}
