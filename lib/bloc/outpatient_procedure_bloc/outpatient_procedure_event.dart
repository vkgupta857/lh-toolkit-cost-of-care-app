part of 'outpatient_procedure_bloc.dart';

abstract class OutpatientProcedureEvent extends Equatable {
  const OutpatientProcedureEvent();
}

class OutpatientProcedureFetchData extends OutpatientProcedureEvent {
  final bool isHardRefresh;
  OutpatientProcedureFetchData({isHardRefresh})
      : isHardRefresh = isHardRefresh ?? false;
  @override
  List<Object> get props => [];
}
